from dataStructure.Deque import Deque
from dataStructure.LinkedList import UnorderedList
from dataStructure.binaryTreeLinked import BinaryTree


# check palindromic str
def palchecker(aString):
    chardeque = Deque()

    for ch in aString:
        chardeque.addRear(ch)

    stillEqual = True

    while chardeque.size() > 1 and stillEqual:
        first = chardeque.removeFront()
        last = chardeque.removeRear()
        if first != last:
            stillEqual = False

    return stillEqual

# print(palchecker("lsdkjfskf"))
# print(palchecker("radar"))


# myTree = ['a', ['b', ['d', [], []], ['e', [], []]], ['c', ['f', [], []], []]]
# print(myTree)
# print('left subtree = ', myTree[1])
# print('root = ', myTree[0])
# print('right subtree = ', myTree[2])

def foo(a,b, *c):
    print(a,b,c)
    print(c[0])
foo(1,2,3)
foo(4,5,None)