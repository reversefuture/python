class People:
    """Summary of class here.

    Longer class information....
    Longer class information....

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    # 类变量,可以通过People.name调用
    name = 'default name'
    age = 0
    # 定义私有属性,私有属性在类外部无法直接进行访问
    __weight = 0
    # 定义构造方法

    def __init__(self, n, a, w):  # self 代表的是类的实例，代表当前对象的地址，而 self.__class__ 则指向类People
        self.name = n
        self.age = a
        self.__weight = w

    def speak(self):
        print("%s 说: 我 %d 岁。" % (self.name, self.age))

# 单继承示例


class Student(People):
    grade = ''

    def __init__(self, n, a, w, g):
        # 调用父类的构函
        People.__init__(self, n, a, w)
        self.grade = g
    # 覆写父类的方法

    def speak(self):
        print("%s 说: 我 %d 岁了，我在读 %d 年级" % (self.name, self.age, self.grade))
    # def __str__(self):#print(s)的首选， __xx__为系统保留的特殊方法，非必要不要这样定义
    #     return 'special'

    def __repr__(self):  # 交互模式下直接打s的首选，__str__没有找__repr__
        return 'raw special'

# s = Student('ken',10,60,3)
# s.speak()
# print(People.name)
# print(s.name)
# print(s)


"""
类的私有变量和私有方法
1、 _xx 以单下划线开头的表示的是protected类型的变量。即保护类型只能允许其本身与子类进行访问。若内部变量标示，如： 当使用“from M import”时，不会将以一个下划线开头的对象引入 。
2、 __xx 双下划线的表示的是私有类型的变量。只能允许这个类本身进行访问了，连子类也不可以,用于命名一个类属性（类变量），调用时名字被改变（在类FooBar内部，__boo变成_FooBar__boo,如self._FooBar__boo）
3、 __xx__定义的是特列方法。用户控制的命名空间内的变量或是属性，如init , __import__或是file 。只有当文档有说明时使用，不要自己定义这类变量。 （就是说这些是python内部定义的变量名）
在这里强调说一下私有变量,python默认的成员函数和成员变量都是公开的,没有像其他类似语言的public,private等关键字修饰.但是可以在变量前面加上两个下划线"_",这样的话函数或变量就变成私有的.这是python的私有变量轧压(这个翻译好拗口),英文是(private name mangling.) **情况就是当变量被标记为私有后,在变量的前端插入类名,再类名前添加一个下划线"_",即形成了_ClassName__变量名.**

Python内置类属性
__dict__ : 类的属性（包含一个字典，由类的数据属性组成）
__doc__ :类的文档字符串
__module__: 类定义所在的模块（类的全名是'__main__.className'，如果类位于一个导入模块mymod中，那么className.__module__ 等于 mymod）
__bases__ : 类的所有父类构成元素（包含了一个由所有父类组成的元组）
"""


class pub():
    _name = 'protected类型的变量'  # 只能允许其本身与子类进行访问
    __info = '私有类型的变量'  # 私有类型的变量和方法 在类的实例中获取和调用不到,只能允许这个类本身进行访问了，连子类也不可以,类的内部为：_pub__info

    def _func(self):  # 在类的实例中可以获取和调用
        print("这是一个protected类型的方法")

    def __func2(self):  # 类的内部为：_pub__func2
        print('这是一个私有类型的方法')

    def get(self):  # 获取私有类型的变量
        return(self.__info)

    def execFunc2(self):
        self.__func2()


a = pub()
print(a._name)  # protected类型的变量
print(pub._name)  # protected类型的变量
a._func()  # 这是一个protected类型的方法
# print(a.info) #AttributeError: 'pub' object has no attribute 'info'
# print(a.__info)  # AttributeError: 'pub' object has no attribute '__info'
# a.__func2() #AttributeError: 'pub' object has no attribute '__func2'
a.execFunc2()  # 这是一个私有类型的方法
a._pub__func2()  # 这是一个私有类型的方法
print(a.get())  # 私有类型的变量
print(dir(a))  # 注意下面结果中最后三个属性
# ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '_pub__func2','_pub__info','_func'...]
print(a.__dict__)  # {}
print(a.__doc__)  # None
print(a.__module__)  # __main__
print(a.__bases__)  # AttributeError: 'pub' object has no attribute '__bases__'
