# 打开一个文件
import os,sys
# f = open("C:/Users/Administrator/Documents/python/base/foo.txt", "r")
abs=os.path.abspath('.') #同 sys.getcwd()
filePath=os.path.join(abs, r'base\foo.txt') #参数中有绝对路径就返回那个绝对路径
print(filePath)
f = open("C:/Users/Administrator/Documents/python/base/foo.txt", "r", 1024, "utf-8")
# str = f.read()
l1=f.readline()
l2=f.readline()
l3=next(f)
print(l1)
print(l2)
print(l3)


# 关闭打开的文件
f.close()