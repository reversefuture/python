def createCounter():
	n = 0
	while(True):
		n = n+1
		yield n
	
counterA = createCounter()
print(next(counterA))

s=0
def closureCounter():
	global s
	s=0
	def inner():
		global s
		s = s + 1
		return s
	return inner

def closureCounter1():
	s=0
	def inner():
		nonlocal s
		s = s + 1
		return s
	return inner


def closureCounter2():
    def f():
        n=0
        while True:
            n=n+1
            yield n
    sun = f()
    def counter():
        return next(sun)
    return counter
 

counterA = closureCounter1()
print(counterA(), counterA(), counterA(), counterA(), counterA()) # 1 2 3 4 5
counterB = closureCounter2()
print(counterB(), counterB(), counterB(), counterB(), counterB()) # 1 2 3 4 5
