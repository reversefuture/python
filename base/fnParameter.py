'''
python的函数参数传递和js一样：都是值传递,对象的值传递的是地址引用
'''

def changeme( mylist ):
   "修改传入的列表"
   mylist.append([1,2,3,4])
   print ("函数内取值: ", mylist)
   return

def changeme2( mylist ):
   "修改传入的列表"
   mylist=[1,2,3,4]
   print ("mylist2函数内取值: ", mylist)
   return

# 调用changeme函数
mylist = [10,20,30]
changeme( mylist )
print ("函数外取值: ", mylist)

# 调用changeme2函数
mylist2 = [10,20,30]
changeme2( mylist2 )
print ("mylist2函数外取值: ", mylist2)