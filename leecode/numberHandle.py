class Solution:
   def romanToInt(self, s: str) -> int:
        rules = {
            'I': 1,
            'V': 5,
            'X': 10,
            'L': 50,
            'C': 100,
            'D': 500,
            'M': 1000,
        }
        special = ['I', 'X', 'C']
        l = list(s)
        cur = 0
        res = 0
        for index, i in enumerate(l):
            if i in special and index+1 < len(l):
                ret = rules[l[index+1]] // rules[i]
                if (ret == 10 or ret == 5):
                    cur = rules[l[index+1]] - rules[i]
                    continue
                else:
                    if cur != 0:
                        res += cur
                        cur = 0
                    else:
                        res += rules[i]
            else:
                if cur != 0:
                    res += cur
                    cur = 0
                else:
                    res += rules[i]
        return res 


slu = Solution()
print(slu.romanToInt('MCMXCIV'))
