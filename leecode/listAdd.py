# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        a1=readList(l1)
        a2=readList(l2)
        s=a1+a2
        return createList(s)
        
def createList(s):
    cur=dummyHead=ListNode(0)
    if(s==0):
        return dummyHead
    while(s>0):
        cur.next=ListNode(s%10)
        cur=cur.next
        s=s//10
    return dummyHead.next

def readList(n):
    r=0
    i=0
    while(n):
        r += n.val* 10**i
        n=n.next
        i +=1
    return r
    

