import functools
import math
import re


class Solution:
    ''' regular expression, use DP动态规划
    https://www.youtube.com/watch?v=l3hda49XcDE&index=17&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr
    d[i][j]: i-each character in s, j-each character in p
1, If p.charAt(j) == s.charAt(i) :  dp[i][j] = dp[i-1][j-1];
2, If p.charAt(j) == '.' : dp[i][j] = dp[i-1][j-1];
3, If p.charAt(j) == '*': 
   here are two sub conditions:
               1   if p.charAt(j-1) != s.charAt(i) : dp[i][j] = dp[i][j-2]  //in this case, a* only counts as empty, eg: 123cde - 123a*cde
               2   if p.charAt(i-1) == s.charAt(i) or p.charAt(i-1) == '.': //*前面字符和s中对应位置匹配，则：
                              dp[i][j] = dp[i-1][j]    //in this case, a* counts as multiple a, eg: 123aaa - 123a*cde
                           or dp[i][j] = dp[i][j-1]   // in this case, a* counts as single a, eg: 123ab - 123a*cde
                           or dp[i][j] = dp[i][j-2]   // in this case, a* counts as empty, eg: 123 - 123a*cde

    '''

    def isMatch(self, s, p):
        n = len(s)
        m = len(p)

        # create 2 d array to store result in boolean form
        # M[i][j] is true if s[:i] matches p[:j] else false
        M = [[False for i in range(m+1)] for i in range(n+1)]

        # empty string matches empty pattern, hence true
        M[0][0] = True

        # if pattern has p = "d*" and s = "" then compute the result for first row
        for i in range(1, m+1):
            if p[i-1] == '*':
                M[0][i] = M[0][i-2]

        # construct DP based on previous values
        for i in range(1, n+1):  # s char
            for j in range(1, m+1):  # p char

                # if there is a char match or pattern has '.' then skip matching char and copy result
                if p[j-1] == '.' or p[j-1] == s[i-1]:
                    M[i][j] = M[i-1][j-1]

                elif p[j-1] == "*":  # if char is '*' then

                    if p[j-2] != s[i-1] and p[j-2] != '.':
                        M[i][j] = M[i][j-2]  # no occurence, copy result
                    else:

                        # one occurance, multiple occurance , no occurance
                        M[i][j] = M[i][j-1] or M[i-1][j] or M[i][j-2]

        return M[-1][-1]

    def isMatch2(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
        def parseP(p):
            res = []
            for index, val in enumerate(p):
                if val != '*':
                    res.append(val)
                else:
                    if len(res) == 0:
                        res[0] = '*'
                    else:
                        res[-1] = res[-1]+'*'
            return res

        l = list(s)
        pl = parseP(p)
        tempPl = pl
        repeatStr = ''
        for index, i in enumerate(pl):
            tempPl.pop(0)
            if len(l) == 0:
                return True

            if i.endswith('*'):
                repeatStr = i[0]
                endStr = pl[index+1][0] if len(pl)-1 > index else ''
                print(l, repeatStr, endStr)
                print(pl)
                if repeatStr == '.':
                    while l != [] and l[0] != endStr:
                        l.pop(0)
                    continue
                elif l[0] == repeatStr:
                    while l != [] and l[0] == repeatStr:
                        l.pop(0)
                    continue
                else:
                    continue

            elif i == '.':
                l.pop(0)
            else:
                if i != l[0]:
                    return False
                else:
                    l.pop(0)

        if len(l) == 0 and len(tempPl) == 0:
            return True
        else:
            return False


slu = Solution()
print(slu.isMatch('aab', 'c*a*b'))
# print(slu.isMatch('mississippi', 'mis*is*p*.'))
# print(slu.isMatch('aaa', 'ab*a*c*a'))
# print(slu.isMatch('abcd', 'd*'))
# print(slu.isMatch('ab', '.*'))
# print(slu.isMatch('ab', '.*c'))
