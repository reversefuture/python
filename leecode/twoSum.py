import functools
import math
import re


class Solution:
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        for index, i in enumerate(nums):
            temp = i
            for index2, j in enumerate(nums[index+1:]):
                if(temp+j == target):
                    return [index, index2 + index + 1]

    # 最长不重复子字符串
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        temp = ''
        re = []
        for index1, i in enumerate(s):
            temp = i
            for j in s[index1+1:]:
                if(j not in temp):  # 新字符不与前面重复
                    temp += j
                else:
                    break
            re.append(temp)  # 把从i起找到的所有子字符串加到re中筛选
        max = functools.reduce(lambda x, y: x if len(x)
                               > len(y) else y, re, '')
        return len(max)

    def findMedianSortedArrays(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: float
        """
        nums1.extend(nums2)
        res = nums1
        if not res:
            return None
        res.sort()
        l = len(res)
        if l % 2:
            return res[l//2] / 1.0
        else:
            return (res[int(l/2)] + res[int(l/2 - 1)]) / 2.0

    # 判断回文数
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        s = str(x)
        if len(s) <= 1:
            return True
        plen = len(s)
        mid = math.ceil(plen/2) - 1
        for index, i in enumerate(s):
            if index > mid:
                return True
            if(i == s[plen-1-index]):
                continue
            else:
                return False

    def isPalindrome2(self, x):
        """
        :type x: int
        :rtype: bool
        """
        if x < 0:
            return False
        else:
            y = str(x)[::-1]
            if y == str(x):
                return True
            else:
                return False

    def findLongestPalindrome(self, s):
        if len(s) <= 1:
            return s
        loc2 = s.rfind(s[0], 1)
        if loc2 > 0:
            start = 0
            end = loc2
            temp = s[start:end+1]
            if self.isPalindromic(temp):
                return temp
            else:
                return self.findMaxStr(s[:loc2])
        else:
            return self.findMaxStr(s[1:])

    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        pstart, pend = 0, 0
        plen = len(s)

        def extend(s, j, k):
            nonlocal pstart, pend
            while j >= 0 and k < len(s) and s[j] == s[k]:
                j -= 1
                k += 1

            if pend-pstart < k-j:
                pstart = j+1  # +1是因为切片的时候是包括开头，不包括结尾的
                pend = k

        if plen < 2:
            return s

        for i in range(plen):
            extend(s, i, i)  # 查找xxyyxx格式的
            extend(s, i, i+1)  # 查找xxyxx格式的
        return s[pstart:pend]

    # convert string so that it will print as Zigzag
    def convert(self, s, numRows):
        """
        :type s: str
        :type numRows: int
        :rtype: str
        """
        plist = ['']*numRows
        row = 0
        addUp = False
        if numRows == 1:
            return s
        for index, i in enumerate(s):
            plist[row] += i
            if row == 0 or row == numRows-1:
                addUp = not addUp
            if addUp:
                row += 1
            else:
                row -= 1
        ret = ''
        for i in plist:
            ret += i
        return ret

    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        ret = 0
        if(x < 0):
            flag = -1
            x = -x
        else:
            flag = 1
        while(abs(x) >= 10):
            n = x % 10
            if n == 0 and abs(ret) == 0:
                x = x//10
                continue
            ret = ret*10+n
            x = x//10
        ret = flag*(ret*10 + x)
        if flag > 0:
            if ret > 2**31-1:
                return 0
        else:
            if ret < 2**31:
                return 0
        return

    def reverse2(self, x):
        """
        :type x: int
        :rtype: int
        """
        max = 2147483647  # int(math.pow(2, 31) - 1)
        min = -2147483648  # int(-math.pow(2, 31))
        label = '' if x > 0 else '-'
        x = abs(x)
        aniStr = str(x)[-1::-1]
        aniStr2 = ''
        if aniStr.startswith('0'):
            for i in aniStr:
                if i == '0' and len(aniStr2) == 0:
                    continue
                aniStr2 += i
        else:
            aniStr2 = aniStr
        if len(aniStr2) == 0:
            return 0
        res = int(label+aniStr2)
        if res > max:
            return 0
        elif res < min:
            return 0
        else:
            return res

    # convert str to integer
    def myAtoi(self, str):
        """
        :type str: str
        :rtype: int
        """
        str = str.strip()
        if len(str) == 0:
            return 0
        p = '^[0-9\-\+]\d*'
        m = re.match(p, str, re.I)
        if not m:
            return 0
        numStr = m.group(0) if not m.group(
            0).startswith('+') else m.group(0)[1:]
        if len(numStr) == 0 or numStr == '-':
            return 0
        num = int(numStr)
        max = 2147483647  # int(math.pow(2, 31) - 1)
        min = -2147483648  # int(-math.pow(2, 31))
        if num > max:
            return max
        elif num < min:
            return min
        else:
            return num


slu = Solution()
# l=s.twoSum([3,1,6,7],9)
# print(l)
# ms='asdfasdffweqfqwmi'
# max=slu.lengthOfLongestSubstring(ms)
# print(max)
# print(slu.findMedianSortedArrays([1,3],[2,4]))
# print(slu.longestPalindrome("ccccb"))
# print(slu.convert('PAYPALISHIRING', 4))
# print(slu.reverse(-1534236469))
# print(slu.myAtoi('42'))
print(slu.isPalindrome(0))
