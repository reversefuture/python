# from . import binaryTreeLinked
# from binaryTreeLinked import *
from binaryTreeLinked import BinaryTree
import operator
import re


def buildParseTree(fpexp):
    # 缺点：每隔操作符两边都需要加()
    # fplist = fpexp.split()
    fplist = splitExpression(fpexp)
    print(fplist)
    pStack = []  # save parent node, the topest is the lowest parent
    eTree = BinaryTree('')
    pStack.append(eTree)
    currentTree = eTree  # current operating node
    for i in fplist:
        # add a new node as the left child of the current node, and descend to the left child.
        if i == '(':
            currentTree.insertLeft('')
            pStack.append(currentTree)
            currentTree = currentTree.getLeftChild()
# set the root value of the current node to the number and return to the parent.
        elif i not in ['+', '-', '*', '/', ')']:
            currentTree.setRootVal(int(i))
            parent = pStack.pop(-1)
            currentTree = parent
            # 处理第一个
# set the root value of the current node to the operator represented by the current token. Add a new node as the right child of the current node and descend to the right child.
        elif i in ['+', '-', '*', '/']:
            currentTree.setRootVal(i)
            currentTree.insertRight('')
            pStack.append(currentTree)
            currentTree = currentTree.getRightChild()
# go to the parent of the current node
        elif i == ')':
            currentTree = pStack.pop(-1)
        else:
            raise ValueError
    return eTree


def buildParseTree2(fpexp):
    '''处理包含and or not的表达式
    '''
    fplist = splitExpression(fpexp)
    print(fplist)
    pStack = []  # save parent node, the topest is the lowest parent
    eTree = BinaryTree('')
    pStack.append(eTree)  # 初始化，根节点设为''
    currentTree = eTree  # current operating node
    for i in fplist:
        # add a new node as the left child of the current node, and descend to the left child.
        if i in ['(']:
            currentTree.insertLeft('')
            pStack.append(currentTree)
            currentTree = currentTree.getLeftChild()
# set the root value of the current node to the number and return to the parent.
        elif i not in ['+', '-', '*', '/', ')', 'and', 'or', 'not']:
            currentTree.setRootVal(int(i))
            parent = pStack.pop(-1)
            if parent.getRootVal == 'not':  # not 是单元操作，需要再返回上一级
                parent = pStack.pop(-1)
            currentTree = parent
# set the root value of the current node to the operator represented by the current token. Add a new node as the right child of the current node and descend to the right child.
        elif i in ['+', '-', '*', '/', 'and', 'or']:
            currentTree.setRootVal(i)
            currentTree.insertRight('')
            pStack.append(currentTree)
            currentTree = currentTree.getRightChild()

        elif i == 'not':  # ((3+2) - not 1 )
            currentTree.setRootVal(i)
            currentTree.insertLeft('')
            pStack.append(currentTree)
            currentTree = currentTree.getLeftChild()
        elif i == ')':
            currentTree = pStack.pop(-1)
        else:
            raise ValueError
    return eTree


def evaluate(parseTree):
    '''求值
    Args:
    Returns:
    Raises:
    '''
    opers = {'+': operator.add, '-': operator.sub,
             '*': operator.mul, '/': operator.truediv}

    leftC = parseTree.getLeftChild()
    rightC = parseTree.getRightChild()

    if leftC and rightC:
        fn = opers[parseTree.getRootVal()]
        return fn(evaluate(leftC), evaluate(rightC))
    else:
        return parseTree.getRootVal()


def printexp2(tree):
    '''还原为中缀表达式,最外面的括号不需要，后面会移除
    Args:
    Returns:
    Raises:
    '''
    sVal = ""
    if tree:
        sVal = '(' + printexp(tree.getLeftChild())
        sVal = sVal + str(tree.getRootVal())
        sVal = sVal + printexp(tree.getRightChild())+')'
    return sVal


def printexp(tree, *isOut):
    '''还原为中缀表达式,最外面的括号不需要，后面会移除
    Args:
    Returns:
    Raises:
    '''
    sVal = ""
    if tree == None:
        return sVal
    if len(isOut) == 0:  # 最外层
        sVal = printexp(tree.getLeftChild(), False)
        sVal = sVal + str(tree.getRootVal())
        sVal = sVal + printexp(tree.getRightChild(), False)
    elif len(isOut) == 1 and isOut[0] == False:
        sVal = '(' + printexp(tree.getLeftChild(), False)
        sVal = sVal + str(tree.getRootVal())
        sVal = sVal + printexp(tree.getRightChild(), False)+')'
    return sVal


def splitExpression(fpexp):
    m = re.findall('([\*\+\/\-\(\)]|\d+|and|or|not)', fpexp)
    return list(m)


pt = buildParseTree("( ( 10 + 5 ) * 3 )")
# pt = buildParseTree2('(3+2 or not 2-2)')
# pt = buildParseTree2('((3+2) and 1)')
# pt = buildParseTree1('((3+2) - not 1 )')
# pt.postorder()
pt.preorder()
print(evaluate(pt))  # 45
print(printexp(pt))  # (((10)+(5))*(3))
# print(pt.preorder())
# print(pt.inorder())
# print(pt.postorder())
# s = "33+2- 11/3*(1+    1)"
# print(splitExpression(s))
