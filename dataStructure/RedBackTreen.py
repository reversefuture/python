class TreeNode:
    def __init__(self, key,color, val, left=None, right=None, parent=None):
        self.key = key
        self.payload = val
        self.leftChild = left
        self.rightChild = right
        self.parent = parent
        self.balanceFactor = 0

    def hasLeftChild(self):
        return self.leftChild

    def hasRightChild(self):
        return self.rightChild

    def isLeftChild(self):
        return self.parent and self.parent.leftChild == self

    def isRightChild(self):
        return self.parent and self.parent.rightChild == self

    def isRoot(self):
        return not self.parent

    def isLeaf(self):
        return not (self.rightChild or self.leftChild)

    def hasAnyChildren(self):
        return self.rightChild or self.leftChild

    def hasBothChildren(self):
        return self.rightChild and self.leftChild

    def replaceNodeData(self, key, value, lc, rc):
        self.key = key
        self.payload = value
        self.leftChild = lc
        self.rightChild = rc
        if self.hasLeftChild():
            self.leftChild.parent = self
        if self.hasRightChild():
            self.rightChild.parent = self

    def replaceWithNode(self, node):
        if node:
            self.replaceNodeData(node.key, node.payload, node.leftChild if hasattr(
                node, 'leftChild') else None, node.rightChild if hasattr(node, 'rightChild') else None)
        else:
            self = None


class AVLTree:
    '''平衡二叉树

    '''

    def __init__(self):
        self.root = None
        self.size = 0

    def length(self):
        return self.size

    def __len__(self):
        return self.size

    def put(self, key, val):
        if self.root:
            self._put(key, val, self.root)
        else:
            self.root = TreeNode(key, val)
        self.size = self.size + 1

    def _put(self, key, val, currentNode):
        if key < currentNode.key:
            if currentNode.hasLeftChild():
                self._put(key, val, currentNode.leftChild)
            else:
                currentNode.leftChild = TreeNode(key, val, parent=currentNode)
                self.updateBalance(currentNode.leftChild)
        else:
            if currentNode.hasRightChild():
                self._put(key, val, currentNode.rightChild)
            else:
                currentNode.rightChild = TreeNode(key, val, parent=currentNode)
                self.updateBalance(currentNode.rightChild)

    def updateBalance(self, node):
        if node.balanceFactor > 1 or node.balanceFactor < -1:
            self.rebalance(node)
            return
        if node.parent != None:
            # balanceFactor=height(leftSubTree)−height(rightSubTree)
            if node.isLeftChild():
                node.parent.balanceFactor += 1
            elif node.isRightChild():
                node.parent.balanceFactor -= 1

            if node.parent.balanceFactor != 0:
                self.updateBalance(node.parent)

    def rotateLeft(self, rotRoot):
        newRoot = rotRoot.rightChild
        rotRoot.rightChild = newRoot.leftChild
        if newRoot.leftChild != None:
            newRoot.leftChild.parent = rotRoot
        newRoot.parent = rotRoot.parent
        if rotRoot.isRoot():
            self.root = newRoot
        else:
            if rotRoot.isLeftChild():
                rotRoot.parent.leftChild = newRoot
            else:
                rotRoot.parent.rightChild = newRoot
        newRoot.leftChild = rotRoot
        rotRoot.parent = newRoot
        rotRoot.balanceFactor = rotRoot.balanceFactor + \
            1 - min(newRoot.balanceFactor, 0)  # 公式推导出的
        newRoot.balanceFactor = newRoot.balanceFactor + \
            1 + max(rotRoot.balanceFactor, 0)

    def rotateRight(self, rotRoot):
        newRoot = rotRoot.leftChild
        rotRoot.leftChild = newRoot.rightChild
        if newRoot.rightChild != None:
            newRoot.rightChild.parent = rotRoot
        newRoot.parent = rotRoot.parent
        if rotRoot.isRoot():
            self.root = newRoot
        else:
            if rotRoot.isRightChild():
                rotRoot.parent.rightChild = newRoot
            else:
                rotRoot.parent.rightChild = newRoot
        newRoot.rightChild = rotRoot
        rotRoot.parent = newRoot
        rotRoot.balanceFactor = rotRoot.balanceFactor + \
            1 - min(newRoot.balanceFactor, 0)  # 公式推导出的
        newRoot.balanceFactor = newRoot.balanceFactor + \
            1 + max(rotRoot.balanceFactor, 0)

# https://blog.csdn.net/saasanken/article/details/80796178
    def rebalance(self, node):
        if node.balanceFactor < 0:  # 右倾斜
            if node.rightChild.balanceFactor > 0:  # 处理右倾斜的右子树左倾斜
                self.rotateRight(node.rightChild)
                self.rotateLeft(node)
            else:
                self.rotateLeft(node)
        elif node.balanceFactor > 0:  # 左倾斜
            if node.leftChild.balanceFactor < 0:  # 处理左倾斜的左子树右倾斜
                self.rotateLeft(node.leftChild)
                self.rotateRight(node)
            else:
                self.rotateRight(node)

    def __setitem__(self, k, v):
        self.put(k, v)

    def get(self, key):
        if self.root:
            res = self._get(key, self.root)
            if res:
                return res.payload
            else:
                return None
        else:
            return None

    def _get(self, key, currentNode):
        if not currentNode:
            return None
        elif currentNode.key == key:
            return currentNode
        elif key < currentNode.key:
            return self._get(key, currentNode.leftChild)
        else:
            return self._get(key, currentNode.rightChild)

    def __getitem__(self, key):
        return self.get(key)

    def __contains__(self, key):
        if self._get(key, self.root):
            return True
        else:
            return False

    def traverse(self):
        result = []
        if self.root == None:
            return result
        temp = [self.root]
        while temp != []:
            curNode = temp.pop(0)
            result.append(curNode.key)
            if curNode.leftChild:
                temp.append(curNode.leftChild)
            if curNode.rightChild:
                temp.append(curNode.rightChild)
        return result

    def myDelete(self, key, *node):
        if self.size > 1:
            if len(node) == 0:
                currentNode = self.root
            else:
                currentNode = node[0]
            nodeToRemove = self._get(key, currentNode)
            if nodeToRemove:
                if nodeToRemove.leftChild and nodeToRemove.rightChild:  # 有2个字节点
                    substitude = self.findMin(
                        nodeToRemove.rightChild)  # 在右子树中找到最小的节点替换
                    nodeToRemove.key = substitude.key
                    nodeToRemove.payload = substitude.payload
                    # 把用来替换的节点在右子树中删掉
                    self.myDelete(substitude.key, nodeToRemove.rightChild)
                else:  # 有一个或0个子节点
                    temp = nodeToRemove
                    if nodeToRemove.leftChild:
                        nodeToRemove.replaceWithNode(nodeToRemove.leftChild)
                    elif nodeToRemove.rightChild:
                        nodeToRemove.replaceWithNode(nodeToRemove.rightChild)
                    else:
                        nodeToRemove.replaceWithNode(None)
                    temp = None

            else:
                raise KeyError('Error, key not in tree')
        elif self.size == 1 and currentNode.key == key:
            currentNode = None
            self.size = self.size - 1
        else:
            raise KeyError('Error, tree is empty')

    def __delitem__(self, key):
        self.myDelete(key)

    def findMin(self, currentNode):
        '''找到当前子树下的最小值并返回
        '''
        current = currentNode
        while current.hasLeftChild():
            current = current.leftChild
        return current


mytree = AVLTree()
# mytree[3] = "red"
# mytree[4] = "blue"
# mytree[6] = "yellow"
# mytree[2] = "at"

# print(mytree[6])
# print(mytree[2])


init = [
    [17, '17'],
    [5, '5'],
    [35, '35'],
    [2, '2'],
    [11, '11'],
    [29, '29'],
    [38, '38'],
    [9, '9'],
    [16, '16'],
    [7, '7'],
    [8, '8']
]

for i in init:
    mytree[i[0]] = i[1]


print(mytree.traverse())
