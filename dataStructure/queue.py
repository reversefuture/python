class Queue:
    #队列顺序存储实现
    def __init__(self, maxSize = 10):
        self.items = [None]*maxSize #初始化, 为了判断方便0位置不会用到
        self.front = 0 #队头，做删除,始终指向队列最前面元素
        self.rear = 0 #队尾，做添加,始终指向队列最后面元素
        self.maxSize = maxSize

    def isEmpty(self):
        return self.front == self.rear

    def isFull(self):
        print((self.rear +1) % self.maxSize)
        return (self.rear +1) % self.maxSize == 0

    def add(self, item):
        if self.isFull():
            print('queue is full')
            return None
        self.rear=(self.rear+1) %self.maxSize
        self.items[self.rear] = item

    def delete(self):
        if self.isEmpty():
            print('queue is empty')
            return None
        self.front = (self.front+1)%self.maxSize
        return self.items.pop(self.front)

    def size(self):
        return len(self.items)
    
    def __str__(self):
        return str(self.items)

class Queue:
    def __init__(self, maxSize = 10):
        self.items = [None]*maxSize #初始化, 为了判断方便0位置不会用到
        print(maxSize)
        self.front = 0 #队头，做删除,始终指向队列最前面元素
        self.rear = 0 #队尾，做添加,始终指向队列最后面元素
        self.maxSize = maxSize

    def isEmpty(self):
        return self.front == self.rear

    def isFull(self):
        print((self.rear +1) % self.maxSize)
        return (self.rear +1) % self.maxSize == 0

    def add(self, item):
        if self.isFull():
            print('queue is full')
            return
        self.rear=(self.rear+1) %self.maxSize
        self.items[self.rear] = item

    def delete(self):
        if self.isEmpty():
            print('queue is empty')
            return
        self.front = (self.front+1)%self.maxSize
        result = self.items[self.front]
        self.items[self.front] = None
        return result

    def size(self):
        return len(self.items)
    
    def __str__(self):
        return str(self.items)

class Node:
    def __init__(self,data):
        self.data = data
        self.next= None

class QueueLinked:
    def __init__(self, maxSize=10):
        self.front=None #front在链表头方便删除
        self.rear=None
        self.maxSize = maxSize
        self.size = 0
    
    def isEmpty(self):
        return self.front == None

    def isFull(self):
        print('size: {0}, maxSize: {1}'.format(self.size, self.maxSize))
        return self.size >= self.maxSize

    def add(self, item):
        if self.isFull():
            print('queue is full')
            return
        node = Node(item)
        if self.isEmpty():
            self.front = node
            self.rear = node
            self.size+=1
            return
        self.rear.next = node
        self.rear = self.rear.next
        self.size+=1

    def delete(self):
        if self.isEmpty():
            print('queue is empty')
            return
        result = self.front
        if self.front == self.rear: #only one ele
            self.front=None
            self.rear = None
        else:
            self.front = self.front.next
        return result.data #result 没被引用了会被回收的

    def size(self):
        return self.size
    
    def __str__(self):
        result = []
        if self.isEmpty():
            return None
        curPoint = self.front
        result.append(curPoint.data)
        while curPoint.next != None:
            curPoint = curPoint.next
            result.append(curPoint.data)
        return str(result)

        


