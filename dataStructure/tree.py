class Node(object):
    def __init__(self, item):
        self.item = item
        self.left = None
        self.right = None

# 模拟完全二叉树的增删查改


class Tree:
    def __init__(self):
        self.curNode = None

    def add(self, item):
        node = Node(item)
        if self.curNode == None:
            self.curNode = node
        else:
            stack = [self.curNode]
            while stack != []:  # while True:
                popNode = stack.pop(0)
                if popNode.left is None:
                    popNode.left = node
                    return
                elif popNode.right is None:
                    popNode.right = node
                    return
                else:
                    stack.append(popNode.left)
                    stack.append(popNode.right)

    def traverse(self):  # 层序遍历
        if self.curNode == None:
            return None
        else:
            stack = [self.curNode]
            res = [self.curNode.item]
            while stack != []:
                popNode = stack.pop(0)
                if popNode.left is not None:
                    stack.append(popNode.left)
                    res.append(popNode.left.item)
                if popNode.right is not None:
                    stack.append(popNode.right)
                    res.append(popNode.right.item)
            return res

    def preOrder(self, popNode):
        if popNode == None:
            return []
        else:
            res = [popNode.item]
            left = self.preOrder(popNode.left)
            right = self.preOrder(popNode.right)
            return res+left+right

    def inOrder(self, root):  # 中序序遍历
        if root is None:
            return []
        result = [root.item]
        left_item = self.inOrder(root.left)
        right_item = self.inOrder(root.right)
        return left_item + result + right_item

    def postOrder(self, root):  # 后序遍历
        if root is None:
            return []
        result = [root.item]
        left_item = self.postOrder(root.left)
        right_item = self.postOrder(root.right)
        return left_item + right_item + result

    def preStackIter(self, root):
        if root is None:
            return []
        stack = []
        res = []
        while stack != [] or root != None:
            while root:
                stack.append(root)
                res.append(root.item)
                root = root.left
            if stack != []:
                popNode = stack.pop()
                if popNode.right:
                    root = popNode.right
        return res

    def inStackIter(self, root):
        if root is None:
            return []
        stack = []
        res = []
        while stack != [] or root != None:
            while root:
                stack.append(root)
                root = root.left
            if stack != []:
                popNode = stack.pop()
                res.append(popNode.item)
                if popNode.right:
                    root = popNode.right
        return res

    # 先按照 中》右》左的顺序遍历，最后再反向输出
    def postStackIter(self, root):
        if root is None:
            return []
        stack = []
        res = []
        while stack != [] or root != None:
            while root:
                stack.append(root)
                res.append(root.item)
                root = root.right
            if stack != []:
                popNode = stack.pop()
                if popNode.left:
                    root = popNode.left
        res.reverse()
        return res

    def postStackIter2(self, root):
        if root is None:
            return []
        stack = []
        res = []
        tempNode = None  # 使用tempNode来记录上一次pop出来的节点，
        while stack != [] or root != None:
            if root:
                stack.append(root)
                root = root.left
            else:
                topNode = stack[-1]
                if topNode.right and topNode.right != tempNode:  # 如果是中间节点，且之前没在tempNode暂存过，那就先访问右边节点
                    root = topNode.right
                else:  # 左边叶子节点，或者之前已经暂存在了tempNode
                    popNode = stack.pop()
                    res.append(popNode.item)
                    tempNode = popNode

        return res


t = Tree()
for i in range(10):
    t.add(i)

t.traverse()

# print(t.preOrder(t.curNode))
# print(t.preStackIter(t.curNode))
# print(t.inOrder(t.curNode))
# print(t.inStackIter(t.curNode))
print(t.postOrder(t.curNode))
print(t.postStackIter2(t.curNode))
