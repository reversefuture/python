from queue import Queue

class Edge: #边
    def __init__(self,v1,v2,weight=1):
        self.v1=v1
        self.v2=v2
        self.weight = weight

class Graph:
    def __init__(self,vNum):
        self.vNum=vNum
        self.eNum=0
        # self.g = [[float('inf')] * vNum] * vNum # * 是潜复制，nested structures are not copied
        self.g = [[float('inf')] * vNum for row in range(vNum)] #有权图，每条边初始化为正无穷
        self.data=[]
        self.visited=[False] * self.vNum

    def insertEdge(self,e):
        self.g[e.v1][e.v2] = e.weight
        self.g[e.v2][e.v1] = e.weight #无向图还需要插入另一边
        
    def isEdge(self,v,w): #v: 行,w： 列
        return self.g[v][w] < float('inf')

    def dfs(self, v): #顶点从0开始
        print('now visiting vertex: ', v)
        # visited=[False] * self.vNum #visited 声明为局部变量每次都是新的，这样循环...
        self.visited[v]= True
        for adj in range(self.vNum): #visit v's adjacent vertex
            if self.g[v][adj] < float('inf') and self.visited[adj] != True:
                self.dfs(adj)

    def bfs(self, v): #顶点从0开始
        q = Queue(self.vNum)
        self.visited[v]= True
        print('now visiting vertex: ', v)
        q.add(v)
        while not q.isEmpty():
            v=q.delete()
            for adj in range(self.vNum): #visit v's adjacent vertex
                if self.g[v][adj] < float('inf') and self.visited[adj] != True:
                    print('now visiting vertex: ', adj)
                    self.visited[adj] = True
                    q.add(adj)
        
    def __str__(self):
        return str(self.g)

class AdjNode: #邻接表的每个边
    def __init__(self,w,weight):
        self.adjV=w
        self.weight = weight
        self.next = None

class VNode: #定点表头节点
    def __init__(self,data=None):
        self.firstEdge = None #firstEdge是邻接表头指针
        self.data = data #存顶点的数据

class GraphLinked:
    def __init__(self,vNum):
        self.vNum=vNum
        self.eNum=0
        self.g = [VNode() for i in range(vNum)] 
        for i in range(vNum): #初始化邻接表头指针
            self.g[i].firstEdge = None
        self.visited=[False] * self.vNum

    def insertEdge(self,e):
        adjNode = AdjNode(e.v2,e.weight)
        adjNode.next = self.g[e.v1].firstEdge
        self.g[e.v1].firstEdge = adjNode

        adjNode2 = AdjNode(e.v1,e.weight)
        adjNode2.next = self.g[e.v2].firstEdge
        self.g[e.v2].firstEdge = adjNode2
        
    def isEdge(self,v,w): #v: 行,w： 列
        return self.g[v][w] < float('inf')

    def dfs(self, v): #顶点从0开始
        print('now visiting vertex: ', v)
        self.visited[v]= True
        w= self.g[v].firstEdge
        while w!= None:
            if self.visited[w.ajdV] != True:
                self.dfs(w.adjV)
            w = w.next

    def bfs(self, v):
        q = Queue(self.vNum)
        print('now visiting vertex: ', v)
        self.visited[v]= True
        q.add(v)
        while not q.isEmpty():
            v=q.delete()
            w= self.g[v].firstEdge
            while w!= None:
                if not self.visited[w.adjV]:
                    print('now visiting vertex: ', w.adjV)
                    self.visited[w.adjV] = True
                    q.add(w.adjV)
                w = w.next

    def getAdjList(self, v):
        result=[]
        w = self.g[v].firstEdge
        while w!= None:
            result.append(w.adjV)
            w = w.next
        return result


    def __str__(self):
        result = [[None] for row in range(self.vNum)]
        for v in range(self.vNum):
            result[v].append(self.getAdjList(v))
        return str(result)



def buildGraph(type):
    print('please input vertex num and edge num: ')
    vNum, eNum = list(map(int, input().strip().split(' ')))
    graph = type(vNum)
    for i in range(eNum):
        print('please input the {0}th edge'.format(i+1))
        v1,v2,weight=list(map(int, input().strip().split(' ')))
        edge = Edge(v1,v2,weight)
        graph.insertEdge(edge)
    
    # print('please inpu vertex data:')
    # for i in range(vNum): #如果定点有数据，读入数据
    #     graph.data.append(int(input().strip()))

    return graph;
        