'''http://interactivepython.org/courselib/static/pythonds/Trees/SearchTreeImplementation.html#fig-bstdel3
'''


class TreeNode:
    def __init__(self, key, val, left=None, right=None, parent=None):
        self.key = key
        self.payload = val
        self.leftChild = left
        self.rightChild = right
        self.parent = parent

    def isLeftChild(self):
        return self.parent and self.parent.leftChild == self

    def isRightChild(self):
        return self.parent and self.parent.rightChild == self

    def isRoot(self):
        return not self.parent

    def isLeaf(self):
        return not (self.rightChild or self.leftChild)

    def hasAnyChildren(self):
        return self.rightChild or self.leftChild

    def hasBothChildren(self):
        return self.rightChild and self.leftChild

    def replaceNodeData(self, key, value, lc, rc):
        self.key = key
        self.payload = value
        self.leftChild = lc
        self.rightChild = rc
        if self.leftChild:
            self.leftChild.parent = self
        if self.rightChild:
            self.rightChild.parent = self

    def replaceWithNode(self, node):
        if node:
            self.replaceNodeData(node.key, node.payload, node.leftChild if hasattr(
                node, 'leftChild') else None, node.rightChild if hasattr(node, 'rightChild') else None)
        else:
            self = None


class BinarySearchTree:
    '''二叉搜索树
        1.非空左子树的所有键值小于其根节点的键值
        2.非空右子树的所有键值大于其根节点的键值
    '''

    def __init__(self):
        self.root = None
        self.size = 0

    def length(self):
        return self.size

    def __len__(self):
        return self.size

    def put(self, key, val):
        if self.root:
            self._put(key, val, self.root)
        else:
            self.root = TreeNode(key, val)
        self.size = self.size + 1

    def _put(self, key, val, currentNode):
        if key < currentNode.key:
            if currentNode.leftChild:
                self._put(key, val, currentNode.leftChild)
            else:
                currentNode.leftChild = TreeNode(key, val, parent=currentNode)
        else:
            if currentNode.rightChild:
                self._put(key, val, currentNode.rightChild)
            else:
                currentNode.rightChild = TreeNode(key, val, parent=currentNode)

    def __setitem__(self, k, v):
        self.put(k, v)

    def get(self, key):
        if self.root:
            res = self._get(key, self.root)
            if res:
                return res.payload
            else:
                return None
        else:
            return None

    def _get(self, key, currentNode):
        if not currentNode:
            return None
        elif currentNode.key == key:
            return currentNode
        elif key < currentNode.key:
            return self._get(key, currentNode.leftChild)
        else:
            return self._get(key, currentNode.rightChild)

    def __getitem__(self, key):
        return self.get(key)

    def __contains__(self, key):
        if self._get(key, self.root):
            return True
        else:
            return False

    def traverse(self):
        result = []
        if self.root == None:
            return result
        temp = [self.root]
        while temp != []:
            curNode = temp.pop(0)
            result.append(curNode.key)
            if curNode.leftChild:
                temp.append(curNode.leftChild)
            if curNode.rightChild:
                temp.append(curNode.rightChild)
        return result

    def delete(self, key):
        if self.size > 1:
            nodeToRemove = self._get(key, self.root)
            if nodeToRemove:
                self.remove(nodeToRemove)
                self.size = self.size-1
            else:
                raise KeyError('Error, key not in tree')
        elif self.size == 1 and self.root.key == key:
            self.root = None
            self.size = self.size - 1
        else:
            raise KeyError('Error, key not in tree')

    def myDelete(self, key, *node):
        if self.size > 1:
            if len(node) == 0:
                currentNode = self.root
            else:
                currentNode = node[0]
            nodeToRemove = self._get(key, currentNode)
            if nodeToRemove:
                if nodeToRemove.leftChild and nodeToRemove.rightChild:  # 有2个字节点
                    substitude = self.findMin(
                        nodeToRemove.rightChild)  # 在右子树中找到最小的节点替换
                    nodeToRemove.key = substitude.key
                    nodeToRemove.payload = substitude.payload
                    # 把用来替换的节点在右子树中删掉
                    self.myDelete(substitude.key, nodeToRemove.rightChild)
                else:  # 有一个或0个子节点
                    temp = nodeToRemove
                    # if not nodeToRemove.leftChild:  # 有右孩子或无子
                    #     nodeToRemove = nodeToRemove.rightChild
                    # elif not nodeToRemove.rightChild:
                    #     nodeToRemove = nodeToRemove.leftChild
                    if nodeToRemove.leftChild:
                        nodeToRemove.replaceWithNode(nodeToRemove.leftChild)
                    elif nodeToRemove.rightChild:
                        nodeToRemove.replaceWithNode(nodeToRemove.rightChild)
                    else:
                        nodeToRemove.replaceWithNode(None)
                    temp = None

            else:
                raise KeyError('Error, key not in tree')
        elif self.size == 1 and currentNode.key == key:
            currentNode = None
            self.size = self.size - 1
        else:
            raise KeyError('Error, tree is empty')

    def __delitem__(self, key):
        self.delete(key)

    def spliceOut(self, currentNode):
        if currentNode.isLeaf():
            if currentNode.isLeftChild():
                currentNode.parent.leftChild = None
            else:
                currentNode.parent.rightChild = None
        elif currentNode.hasAnyChildren():
            if currentNode.leftChild:
                if currentNode.isLeftChild():
                    currentNode.parent.leftChild = currentNode.leftChild
                else:
                    currentNode.parent.rightChild = currentNode.leftChild
                currentNode.leftChild.parent = currentNode.parent
            else:
                if currentNode.isLeftChild():
                    currentNode.parent.leftChild = currentNode.rightChild
                else:
                    currentNode.parent.rightChild = currentNode.rightChild
                currentNode.rightChild.parent = currentNode.parent

    def findSuccessor(self, currentNode):
        succ = None
        if currentNode.rightChild:
            succ = self.findMin(currentNode.rightChild)
        else:
            if currentNode.parent:
                if currentNode.isLeftChild():
                    succ = currentNode.parent
                else:
                    currentNode.parent.rightChild = None
                    succ = currentNode.parent.findSuccessor()
                    currentNode.parent.rightChild = currentNode
        return succ

    def findMin(self, currentNode):
        '''找到当前子树下的最小值并返回
        '''
        current = currentNode
        while current.leftChild:
            current = current.leftChild
        return current

    def remove(self, currentNode):
        if currentNode.isLeaf():  # leaf
            if currentNode == currentNode.parent.leftChild:
                currentNode.parent.leftChild = None
            else:
                currentNode.parent.rightChild = None
        elif currentNode.hasBothChildren():  # interior
            succ = self.findSuccessor(currentNode)
            self.spliceOut(succ)
            currentNode.key = succ.key
            currentNode.payload = succ.payload

        else:  # this node has one child
            if currentNode.leftChild:
                if currentNode.isLeftChild():
                    currentNode.leftChild.parent = currentNode.parent
                    currentNode.parent.leftChild = currentNode.leftChild
                elif currentNode.isRightChild():
                    currentNode.leftChild.parent = currentNode.parent
                    currentNode.parent.rightChild = currentNode.leftChild
                else:
                    currentNode.replaceNodeData(currentNode.leftChild.key,
                                                currentNode.leftChild.payload,
                                                currentNode.leftChild.leftChild,
                                                currentNode.leftChild.rightChild)
            else:
                if currentNode.isLeftChild():
                    currentNode.rightChild.parent = currentNode.parent
                    currentNode.parent.leftChild = currentNode.rightChild
                elif currentNode.isRightChild():
                    currentNode.rightChild.parent = currentNode.parent
                    currentNode.parent.rightChild = currentNode.rightChild
                else:
                    currentNode.replaceNodeData(currentNode.rightChild.key,
                                                currentNode.rightChild.payload,
                                                currentNode.rightChild.leftChild,
                                                currentNode.rightChild.rightChild)


class MyTreeNode:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class MyBinarySearchTree:
    def __init__(self, maxSize):
        self.root = None
        self.size = 0
        self.maxSize = maxSize

    def find(self, item):
        if self.root == None:
            return None
        currentNode = self.root
        if currentNode:
            if item > currentNode.data:
                currentNode = currentNode.right
            elif item < currentNode.data:
                currentNode = currentNode.left
            else:
                return currentNode

    def insert(self, item):
        if self.root == None:
            node = MyTreeNode(item)
            self.root = node
        cur = self.root
        while cur:
            if item >


mytree = BinarySearchTree()
# mytree[3] = "red"
# mytree[4] = "blue"
# mytree[6] = "yellow"
# mytree[2] = "at"

# print(mytree[6])
# print(mytree[2])


init = [
    [17, '17'],
    [5, '5'],
    [35, '35'],
    [2, '2'],
    [11, '11'],
    [29, '29'],
    [38, '38'],
    [9, '9'],
    [16, '16'],
    [7, '7'],
    [8, '8']
]

for i in init:
    mytree[i[0]] = i[1]

print(mytree[6])


print(mytree.traverse())
# mytree.delete(5)
# mytree.myDelete(5)
print(mytree.myFind(5))
print(mytree.traverse())
