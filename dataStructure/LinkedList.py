class Node:
    def __init__(self, initdata):
        self.data = initdata
        self.next = None


class UnorderedList:

    def __init__(self):
        self.head = None

    def isEmpty(self):
        return self.head == None

    def add(self, item):  # 头部添加
        temp = Node(item)
        temp.next = self.head
        self.head = temp

    def size(self):
        current = self.head
        count = 0
        while current != None:
            count = count + 1
            current = current.next

        return count

    def search(self, item):
        current = self.head
        found = False
        while current != None and not found:
            if current.data == item:
                found = True
            else:
                current = current.next

        return found

    def remove(self, item):
        current = self.head
        previous = None
        found = False
        while not found:
            if current.data == item:
                found = True
            else:
                previous = current
                current = current.next

        if previous == None:  # 处理item在head的情况
            self.head = current.next
        else:
            previous.next = current.next

    def append(self, item):
        if self.head == None:
            self.add(item)
        node = Node(item)
        current = self.head
        while current.next != None:
            current = current.next

        current.next = node

    def pop(self):  # 从结尾pop
        if self.head == None:
            return None
        current = self.head
        while current.next.next != None:  # 获得倒数第二个node
            current = current.next

        val = current.next.data
        current.next = None
        return val

    def insert(self, index, item):
        if self.head == None:
            self.add(item)
        i = 0
        node = Node(item)
        current = self.head
        while i <= index-2:  # 在将插入位置前一个位置停下
            current = current.next
            i += 1

        next = current.next
        current.next = node
        current.next.next = next

    def index(self, item):
        if self.head == None:
            return -1
        i = 0
        current = self.head
        while current.data != item:
            current = current.next
            i += 1

        if current != None:
            return i
        else:
            return -1

    def getAll(self):
        if self.head == None:
            return []
        res = []
        current = self.head
        while current != None:
            res.append(current.data)
            current = current.next

        return res


class OrderedList:
    '''有序，从小到大，插入需要先查找位置

    '''

    def __init__(self):
        self.head = None

    def search(self, item):
        current = self.head
        found = False
        stop = False
        while current != None and not found and not stop:
            if current.data == item:
                found = True
            else:
                if current.data > item:  # 找到了比item大的节点还没找到，停止
                    stop = True
                else:
                    current = current.next

        return found

    def add(self, item):
        current = self.head
        previous = None
        stop = False
        while current != None and not stop:
            if current.data > item:
                stop = True
            else:
                previous = current
                current = current.next

        temp = Node(item)
        if previous == None:
            temp.next = self.head
            self.head = temp
        else:
            temp.next = current
            previous.next = temp

    def isEmpty(self):
        return self.head == None

    def size(self):
        current = self.head
        count = 0
        while current != None:
            count = count + 1
            current = current.next

        return count


# mylist = OrderedList()
# mylist.add(31)
# mylist.add(77)
# mylist.add(17)
# mylist.add(93)
# mylist.add(26)
# mylist.add(54)

# print(mylist.size())
# print(mylist.search(93))
# print(mylist.search(100))
