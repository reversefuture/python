class BinHeap:
    '''最小堆操作，增加元素都是从下(最后父节点)往上调整，
    删除max/min元素都是把最后的元素放到删除元素位置然后从上往下调整,动了哪儿就从哪儿开始
    最大堆操作类似，不同的是父比子大
    '''

    def __init__(self):
        self.heapList = [0]  # 哨兵
        self.currentSize = 0

    def percUp(self, i):
        '''从下往上调整,没有考虑兄弟节点？
        '''
        while i // 2 > 0:
            if self.heapList[i] < self.heapList[i // 2]:
                tmp = self.heapList[i // 2]
                self.heapList[i // 2] = self.heapList[i]
                self.heapList[i] = tmp
            i = i // 2

    def insert(self, k):
        self.heapList.append(k)
        self.currentSize = self.currentSize + 1
        self.percUp(self.currentSize)

    def percDown(self, i):
        '''从上往下调整
        '''
        while (i * 2) <= self.currentSize:
            mc = self.minChild(i)
            if self.heapList[i] > self.heapList[mc]:
                tmp = self.heapList[i]
                self.heapList[i] = self.heapList[mc]
                self.heapList[mc] = tmp
            i = mc

    def minChild(self, i):
        if i * 2 + 1 > self.currentSize:
            return i * 2
        else:
            if self.heapList[i*2] < self.heapList[i*2+1]:
                return i * 2
            else:
                return i * 2 + 1

    def delMin(self):
        retval = self.heapList[1]
        self.heapList[1] = self.heapList[self.currentSize]
        self.currentSize = self.currentSize - 1 #此时在1-和currentsize有2个最大的元素
        self.heapList.pop()
        self.percDown(1)
        return retval

    def buildHeap(self, alist):
        '''构建堆，从倒数第二层的最后一个父节点开始往上调整
        '''
        i = len(alist) // 2
        self.currentSize = len(alist)
        self.heapList = [0] + alist[:]
        while (i > 0):
            self.percDown(i)
            i = i - 1


bh = BinHeap()
bh.buildHeap([1, 3, 4, 2, 6, 7, 8])
print(bh.heapList)
print(bh.delMin())
print(bh.delMin())
print(bh.delMin())
print(bh.delMin())
print(bh.delMin())

