from flask import Flask
from flask import request
app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/login', methods=['POST', 'GET'])
def login():
    error = None
    if request.method == 'POST':
        if valid_login(request.form['username'],
                       request.form['password']):
            return 'Welcome ' + request.form['username']
        else:
            error = 'Invalid username/password'
            return error
    # the code below is executed if the request method
    # was GET or the credentials were invalid
    # return render_template('login.html', error=error)


def valid_login(name, pwd):
    print(name, pwd)
    if name == 'admin' and pwd == '123456':
        return True
    return False


if __name__ == '__main__':
    app.run()
