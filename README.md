# python

#### 项目介绍
test

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明
run flaskDemo.py
##pip3 install virtualenv
virtualenv 安装完毕后，你可以立即打开 shell 然后创建你自己的环境。我通常创建一个项目文件夹，并在其下创建一个 venv 文件夹

##$ mkdir myproject
##$ cd myproject
##$ virtualenv venv
新建的Python环境被放到当前目录下的venv目录。有了venv这个Python环境，可以用source进入该环境：
let's activate the virtualenv

##source ./env/Scripts/activate
在venv环境下，用pip安装的包都被安装到venv这个环境下，系统Python环境不受任何影响。也就是说，venv环境是专门针对myproject这个应用创建的。
注意到命令提示符变了，有个(venv)前缀，表示当前环境是一个名为venv的Python环境。
安装flask到当前app中：
##pip install flask
and run it with

##python ./appDemo/flaskDemo.py

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


##参考
[http://interactivepython.org/courselib/static/pythonds/index.html](http://interactivepython.org/courselib/static/pythonds/index.html)
[https://www.icourse163.org/](https://www.icourse163.org/)