const { fork } = require('child_process')
const fd = fork('./sub.js')
fd.stdout.on('data', data => console.log(`In parent: ${data}`))