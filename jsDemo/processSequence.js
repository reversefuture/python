console.log('start') //宏观1
process.nextTick(function () { //微观1
    console.log('nextTick')
})

Promise.resolve().then(function () { //微观1
    console.log('promise1')
})
var promise2 = new Promise(function (resolve, reject) { //宏观1
    console.log('promise2');
    resolve('promise2 resolve')
})
promise2.then(function (data) { //微观1
    console.log(`Promise2 then: ${data}`)
})
setTimeout(function () { //宏观2
    console.log('setTimeImmediate')
}, 0)
console.log('end')