function Animal(color, age = 0) {
    this.color = color;
    this.age = age;

}
Animal.prototype.say = function () {
    console.log(this)
    console.log(this.__proto__.__proto__ == Animal.prototype) //true
    console.log(`A ${this.color} ${this.constructor.name} say...`) //实例的原型存放在__proto__，但是引用__proto__中的属性不要加它
}

function Dog(color, age = 0, wight) {
    Animal.call(this, color, age);
    this.wight = weight;
}
Dog.prototype = Object.create(Animal.prototype)
Dog.prototype.constructor = Dog;
Object.defineProperty(Dog.prototype.constructor, 'name', {
    value: 'Cat'
})


Function.prototype.bind = function (obj, name) {
    var that = this;
    eval(`var result= function ${name}() {that.apply(this,Array.prototype.slice.call(arguments));}`)
    result.prototype = Object.create(that.prototype);
    result.prototype.constructor = result;
    return result;
}

Function.prototype.bind2 = function (context, name) {
    var args = Array.prototype.slice(arguments, 1), //预置参数
        F = function () {}, //用来存放原型链
        self = this, //被bind的函数
        bound = function () { //返回的新函数
            var innerArgs = Array.prototype.slice.call(arguments);
            var finalArgs = args.concat(innerArgs);
            //直接调用函数那样调用新构造函数时把this设置为context, 防止变成global，用new FunctionName()来调用时this默认指向父构造函数
            // return self.apply((this instanceof F ? this : context), finalArgs); //继承方法1调用方式
            return self.apply(this, finalArgs) //继承方法2调用方式
        };
    //继承后bound.__proto__.__proto__==Animal.prototype
    //继承方法1：
    // F.prototype = self.prototype;
    // bound.prototype = new F();
    //继承方法2：
    bound.prototype = Object.create(self.prototype)
    bound.prototype.constructor = bound;
    // bound.prototype.constructor.name = name;
    Object.defineProperty(bound.prototype.constructor, 'name', {
        value: name
    })
    return bound;
};


const Cat = Animal.bind(null, 'Cat');
var animal = new Animal('red');
animal.say()
var cat = new Cat('white');
cat.say();
console.log(cat instanceof Cat)
console.log(cat instanceof Animal)

const Lion = Animal.bind2(null, 'Lion');
var lion = new Lion('yellow');
lion.say();
console.log(lion instanceof Lion)
console.log(lion instanceof Animal)