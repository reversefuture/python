const http = require('http')
const url = require('url')
const path = require('path')
const fork = require('child_process').fork;

http.createServer(function (request, response) {
    const worker = fork(path.resolve(__dirname, './fibo.js')) //创建一个工作进程
    let Urlparsed = url.parse(request.url);
    let r = /n=(\d*)/ig;
    let m = r.exec(Urlparsed.query);
    let n = m ? m[1] : 1;
    worker.on('message', function (m) { //接收工作进程计算结果
        if ('object' === typeof m && m.type === 'fibo') {
            worker.kill(); //发送杀死进程的信号
            response.writeHead(200, { 'Content-Type': 'text/plain' });

            // 发送响应数据, 只接受string or Buffer
            response.end(m.result.toString());
        }
    });
    worker.send({ type: 'fibo', num: String(n) }); //发送消息给工作进程
    //发送给工作进程计算fibo的数量
}).listen(8888);

// 终端打印如下信息
console.log('Server running at http://127.0.0.1:8888/');