# 给定一个m行n列的二维地图, 初始化每个单元都是水.
# 操作addLand 把单元格(row,col)变成陆地.
# 岛屿定义为一系列相连的被水单元包围的陆地单元, 横向或纵向相邻的陆地称为相连(斜对角不算).
# 在一系列addLand的操作过程中, 给出每次addLand操作后岛屿的个数.
# 二维地图的每条边界外侧假定都是水.

##
# 问题解决思路的核心就一句话：
#     numOfIsland=numOfIsland+1-countBridge(row,col)
# 加入一个岛后岛的数目等于原来岛数目加1减去这个岛带来的桥的数目（该岛与其他岛相连称为有1个桥）

import sys
  
m=int(sys.stdin.readline().strip())
n=int(sys.stdin.readline().strip())
k=int(sys.stdin.readline().strip())
arr=[[0 for i in range(n)]for i in range(m)]

def inArray(r,c):
    if r<0 or r==m or c<0 or c==n:
        return 0
    return arr[r][c]

def countBridge(row,col):
    return inArray(row-1,col)+inArray(row+1,col)+inArray(row,col-1)+inArray(row,col+1)

def addLand(row,col,numOfIsland):
    arr[row][col]=1
    numOfIsland=numOfIsland+1-countBridge(row,col)
    if numOfIsland==0:
        numOfIsland=1
    sys.stdout.write(str(numOfIsland)+' ')
    return numOfIsland


numOfIsland=0
while True:
    line= sys.stdin.readline().strip()
    if len(line)==0:break #一直输入，遇到空行结束
    [row,col]=map(int,line.split())
    if row<0 or row>m-1 or col<0 or col >n-1: #过滤输入
        sys.stdout.write(str(numOfIsland)+' ')
        continue
    if arr[row][col]==1: #已经输入过了
        sys.stdout.write(str(numOfIsland)+' ')
    else:
        numOfIsland=addLand(row,col,numOfIsland)