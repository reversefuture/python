
# 参考 https://www.cnblogs.com/onepixel/articles/7674659.html
# https://www.icourse163.org/learn/ZJU-93001?tid=1003013004#/learn/content?type=detail&id=1004242228&sm=1

# 插入排序,从小到大


def insertion_sort(plist):
    l = len(plist)
    for i in range(1, l):
        preIndex = i-1  # 记录前面待比较的元素index
        current = plist[i]  # 摸出下一张牌
        # 如果前面元素》current，把前面元素后移
        while preIndex >= 0 and plist[preIndex] > current:
            plist[preIndex+1] = plist[preIndex]
            preIndex -= 1
        plist[preIndex+1] = current  # 找到了《=current的前面元素，插入current
    return plist

# 选择排序


def selection_sort(plist):
    l = len(plist)
    for i in range(0, l):
        pos = i  # 记录本轮最小值坐标
        for j in range(i+1, l):
            if plist[j] < plist[pos]:  # 当前值 < current，改变pos
                pos = j
        if pos != i:
            plist[i], plist[pos] = plist[pos], plist[i]
    return plist

# 冒泡排序


def bubble_sort(plist):
    l = len(plist)
    for i in range(l-1, 0, -1):
        isSorted = False  # 记录上一轮是否已经交换过，没交换过说明是排好了的
        if not isSorted:
            for j in range(0, i):
                if plist[j] > plist[j+1]:
                    plist[j], plist[j+1] = plist[j+1], plist[j]
                    isSorted = False
        else:
            continue
    return plist

 # 冒泡排序改进


def bubble_sort2(plist):
    l = len(plist)
    i = l-1
    while i >= 0:
        # 记录上一轮最后交换的地址，lastSwap->end是已经排序好了的，所以i应该从0->lastSwap
        lastSwap = 0  # 每一轮要初始化为0，防止某一轮未发生交换，lastSwap保留上一轮的值进入死循环
        for j in range(0, i):
            if plist[j] > plist[j+1]:
                plist[j], plist[j+1] = plist[j+1], plist[j]
                lastSwap = j  # 最后一次交换位置的坐标
        if lastSwap == 0:
            i -= 1
        else:
            i = lastSwap

    return plist

# 堆排序是对选择排序的改进，加快了内层循环查找最小元素的速度


def heap_sort1(plist):  # O(N* logN)
    BuildHeap(plist)  # 建立最小堆 O(N)
    for i in range(0, len(plist)):
        TempA[i] = DeleteMin(plist)  # 每次从最小堆中取出最小的存入临时数组TempA O(logN)
    for i in range(0, len(plist)):  # 把tempA导出到plist,需要额外的 O(N)空间
        plist[i] = TempA[i]


def heap_sort(plist):
    for i in range(0, len(plist)):  # BuildHeap建立最大堆， plist[0]不存放哨兵而是元素
        PercDown(plist, i, len(plist))

    for i in range(0, len(plist)):  # 每次把plist的0和最后位置的元素交换，然后把从0 -> i的元素重新建堆
        plist = swap(plist, 0, i)
        PercDown(plist, 0, i)
    return plist


def PercDown(plist, parent, size):
    #: 存放待调整的子堆的根
    l = size
    while parent*2+1 < l:  # 取出较大的放在parent
        child = parent*2+1
        if child+1 < l and plist[child] < plist[child+1]:
            child = child+1
        if plist[child] > plist[parent]:
            plist[parent], plist[child] = plist[child], plist[parent]
            parent = child
        else:
            parent = child
            continue


def swap(plist, index1, index2):
    plist[index1], plist[index2] = plist[index2], plist[index1]
    return plist


# 快速排序首先找到一个基准（pivot）
# ，然后先从右向左搜索，如果发现比pivot小，则和pivot交换，然后从左向右搜索，如果发现比pivot大，则和pivot交换，一直到左边大于右边，此时pivot左边的都比它小，而右边的都比它大，
# 此时pivot的位置就是排好序后应该在的位置，此时pivot将数组划分为左右两部分，可以递归采用该方法进行。快排的交换使排序成为不稳定的。
# pivot取到最中间的情况下速度最快

l = [1, 3, 4, 2, 6, 7, 8]
print(heap_sort(l))
