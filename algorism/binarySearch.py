import math
# binary search while implementation
def binarySearch1(plist,item):
    if len(plist) ==0:
        return -1
    left,right = 0, len(plist)-1
    idx=-1
    while left<=right:
        mid= (right+left)//2
        if plist[mid] == item:
            idx=mid
            break
        elif plist[mid] > item:
            right=mid-1
        elif plist[mid] < item:
            left=mid+1

    return idx

# binary search recursion implementation
def binarySearch2(plist,item):
    if len(plist) ==0:
        return -1
    left,right = 0, len(plist)-1
    mid= (right+left)//2

    if plist[mid] == item:
        return mid
    elif plist[mid] > item:
        right=mid-1
        return binarySearch2(plist[left:right+1], item)
    elif plist[mid] < item:
        left=mid+1
        return binarySearch2(plist[left:right+1], item) + mid+1
    return -1

mylist=[1,2,3,4,5]
print(binarySearch2(mylist,5))
