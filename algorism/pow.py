# -*- coding:utf-8 -*-
#给定两个数R和n，输出R的n次方，其中0.0<R<99.999, 0<n<=25
import re
def float_format(result_int, dec_num):
    result_str = str(result_int)
    if len(result_str) <= dec_num:
        result = '%0{}d'.format(dec_num) % result_int
        result = '0.' + result
    else:
        left_result = result_str[:-dec_num]
        right_result = result_str[-dec_num:]
        result = '.'.join([left_result, right_result])
    return result.rstrip('0').rstrip('.')
 
a = input()
row_list = a.split('\n')
for row in row_list:
    # re.split(r'\s+', text)；将字符串按空格分割成一个单词列表。
    a = re.split('\s+', row)[0]
    b = re.split('\s+', row)[1]
    str_len = len(a)
    # 找到小数点的位置
    point_pos = a.find('.')
    # 小数的长度
    dec_num = str_len - 1 - point_pos
    a_int = int(a.replace('.', ''))
    b_int = int(b)
    result_int = a_int ** b_int
    result = float_format(result_int, dec_num * int(b))
    print(result)