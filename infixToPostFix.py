import re
def infixToPostfix(infixexpr):
    precedence = {
        '*': 3,
        '/': 3,
        '+': 2,
        '-': 2,
        '(': 1  # 坐括号肯定比它里面的符号优先级低
    }


    postfixStr=''
    infixList = infixexpr.split()
    oprStack=[]

    for i in infixList:
        print(i)
        print(oprStack)
        oprLen=len(oprStack)
        if re.search(r'\d+|\w+', i):
            postfixStr+=i
        elif i=='(':
            oprStack.append(i)

        elif i==')':
            popToken=oprStack.pop()
            while(popToken!='('):
                postfixStr+=popToken
            # postfixStr+=popToken 不需要(   
        else:
            while (len(oprStack) >0) and \
               (precedence[oprStack[oprLen-1]] >=precedence[i]):
                  postfixStr+=oprStack.pop()
            oprStack.append(i)
    while len(oprStack)>0:
        postfixStr+=oprStack.pop()

    return postfixStr


print(infixToPostfix("A * B + C * D"))
print(infixToPostfix("1+2*(3+4/2)"))