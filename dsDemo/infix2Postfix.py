import re


def infix2Postfix(infixStr):
    prec = {}  # precedence
    prec['*'] = 3
    prec['/'] = 3
    prec['+'] = 2
    prec['-'] = 2
    prec['('] = 1
    opStack = []
    postfixList = []
    tokenList = infixStr.split()
    for token in tokenList:
        if re.search('[A-Z|0-9]', token):
            postfixList.append(token)
        elif token == '(':
            opStack.append(token)
        elif token == ')':
            topToken = opStack.pop()
            while topToken != '(':
                postfixList.append(topToken)
                topToken = opStack.pop()
        else:
            while opStack != [] and (prec[opStack[-1]] >= prec[token]):
                postfixList.append(opStack.pop())
            opStack.append(token)

    while opStack != []:
        postfixList.append(opStack.pop())
    return " ".join(postfixList)


def evaluatePostfix(postfixStr):
    if postfixStr == '':
        return 'empty input'
    elif re.search('[0-9\+\-\*\/\s]+', postfixStr) == None:
        return 'include invalid char'
    result = 0
    strStack = postfixStr.split(' ')
    oprandStack = []
    print(strStack)
    for token in strStack:
        if re.search('[0-9]+', token):
            oprandStack.append(token)
        else:
            num2 = oprandStack.pop()
            num1 = oprandStack.pop()
            exp = str(num1)+str(token)+str(num2)
            print(exp)
            oprandStack.append(eval(exp))
    return oprandStack.pop()


# print(infix2Postfix("A * B + C * D"))
# print(infix2Postfix("( A + B ) * C - ( D - E ) * ( F + G )"))
print(evaluatePostfix('7 8 + 3 2 + /'))
