#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
判断1000以内的素数
计算素数的一个方法是埃氏筛法，它的算法理解起来非常简单：

首先，列出从2开始的所有自然数，构造一个序列：

2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, …

取序列的第一个数2，它一定是素数，然后用2把序列的2的倍数筛掉：

3, 5, 7, 9, 11, 13, 15, 17, 19, …

取新序列的第一个数3，它一定是素数，然后用3把序列的3的倍数筛掉：

5, 6, 7, 8, 10, 11, 13, 14, 16, 17, 19, 20, …

取新序列的第一个数5，然后用5把序列的5的倍数筛掉：

7, 8, 9, 11, 12, 13, 14, 16, 17, 18, 19, …

不断筛下去，就可以得到所有的素数。
'''

def main():
    for n in primes():
        if n < 20:
            print(n)
        else:
            break

#定义一个生成器，不断返回下一个素数
def _odd_iter():
    n = 1
    while True:
        n = n + 2 # 3 5 7...序列已经包含后面所有素数，+2排出2
        yield n

#筛选函数
def _not_divisible(n):
    return lambda x: x % n > 0

#这个生成器先返回第一个素数2，然后，利用filter()不断产生筛选后的新的序列
def primes():
    yield 2
    it = _odd_iter() # 初始序列
    while True:
        n = next(it) # 返回序列的第一个数
        yield n
        it = filter(_not_divisible(n), it) # 构造新序列
        # it = map(lambda x:x+0.1, it) # 构造新序列

if __name__ == '__main__':
    main()
