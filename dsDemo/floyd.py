import sys
sys.path.append("..") # depending on current executing context.
from dataStructure.map import Graph,Edge
from dataStructure.map import Graph,Edge

def floyd(graph,D,path):
    '''邻接矩阵存储 - 有权图的源最短路算法
    graph: 图
    D:   记录每个原点到每个顶点的距离
    path:   记录从每个原点到达当前i节点的最短路径的上一个节点下标
    '''
    print(graph)
    for i in range(graph.vNum):
        for j in range(graph.vNum):
            D[i][j] = graph.g[i][j]
            path[i][j]=-1

    for k in range(graph.vNum):
        for i in range(graph.vNum):
            for j in range(graph.vNum):
                if D[i][k] + D[k][j] < D[i][j]:
                    D[i][j] = D[i][k] + D[k][j]
                    if i==j and D[i][j]<0: #若发现负值圈
                        return False #不能解决
                    path[i][j]=k #记录路径
    print(path)
    print(D)
    return True

graph = Graph(7)
graph.insertEdge(Edge(0,1,2))
graph.insertEdge(Edge(0,3,1))
graph.insertEdge(Edge(1,3,3))
graph.insertEdge(Edge(1,4,10))
graph.insertEdge(Edge(2,0,4))
graph.insertEdge(Edge(2,6,5))
graph.insertEdge(Edge(3,2,2))
graph.insertEdge(Edge(3,4,2))
graph.insertEdge(Edge(3,6,4))
graph.insertEdge(Edge(3,5,8))
graph.insertEdge(Edge(4,6,6))
graph.insertEdge(Edge(6,5,1))

dist=[[float('inf')]*7 for row in range(7)]
path=[[-1]*7 for row in range(7)]
floyd(graph,dist,path)