# 构建矩阵，0代表不认识，不认识
# 从一个点开始遍历，遇到认识的加入队列，一层遍历完后向外扩展，求遍历6层后认识的人数比率。

import collections
pin = input().split(' ')
m, n = map(int, pin)

cur = 0

graph = [[0 for i in range(m)] for i in range(m)]

while cur < n:
    curin = input().split(' ')
    a, b = map(int, curin)
    graph[a-1][b-1] = 1
    cur += 1
print(graph)


def BFS(v):
    global graph
    visited = [False for i in range(m)]
    d = collections.deque()
    visited[v-1] = True
    count = 1
    level = 0
    last = v  # the last visited id of this level
    tail = -1

    d.append(v)  # save the person Id
    while len(d) != 0:
        v = d.popleft()
        for index, i in enumerate(graph[v-1]):
            if i == 0:
                continue
            if not visited[index]:
                visited[index] = True
                d.append(index+1)
                count += 1
                tail = index+1
        if v == last:  # if the last pop id equal the saved one
            level += 1
            last = tail
        if level == 6:
            break
    return count


for i in range(m):
    count = BFS(i+1)
    print('{:.2%}'.format(round(count/m, 4)))
