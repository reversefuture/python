import math
'''
给定一串数字，构建完全二叉搜索树。左<根<右。
选取数组存储树，因为是完全的。从数组的0位置开始存放
'''
A=[]
T=[]

def getLeftLength(n):
    '''
    假设有h层的完全二叉树，左子树的最后一层有x个，总个数N=2^h - 1 + x, 满二叉树总个数:2^H-1
    H=|log2(N+1)|
    '''
    h = math.floor(math.log(n+1, 2))
    x= n - math.pow(2, h) +1
    x = min(x, math.pow(2,h-1))
    l = math.pow(2,h-1) -1 + x
    return int(l)

def solve(Aleft,ARight,TRoot):
    '''相当于不断找到根节点位置依次放入

    Aleft:  当前处理子数组左边位置
    ARight: 当前处理子数组右边位置
    TRoot:  当前待填入的根节点在完全二叉树中位置，初始0
    '''
    global A,T
    n=ARight -Aleft+1 #当前处理子数组个数
    if n==0:
        return
    leftNum = getLeftLength(n) #左子树个数
    print(leftNum)
    T[TRoot] = A[Aleft+leftNum]
    LeftRoot = TRoot*2+1
    RightRoot = LeftRoot +1
    solve(Aleft, Aleft+leftNum-1, LeftRoot)
    solve(Aleft+leftNum+1,ARight,RightRoot)


A=[0,1,2,3,4,5,6,7,8,9]
T = [None]*10
solve(0,9,0)
print(T)

