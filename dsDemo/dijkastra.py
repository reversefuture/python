import sys
sys.path.append("..") # depending on current executing context. must in current directory
from dataStructure.map import Graph,Edge
from dataStructure.map import Graph,Edge

def findMinDist(graph,dist,collected):
    minDist=float('inf')

    for v in range(graph.vNum):
        if collected[v] ==False and dist[v]<minDist:
            minDist = dist[v]
            minV = v
        
    if minDist< float('inf'):
        return minV
    else:
        return None


def dijkastra(graph,dist,path,s):
    '''邻接矩阵存储 - 有权图的单源最短路算法
    graph: 图
    dist:   记录原点到每个顶点的距离
    path:   记录从原点到达当前i节点的最短路径的上一个节点下标
    s:      起点
    '''
    collected = [False]*graph.vNum

    #初始化：此处默认邻接矩阵中不存在的边用INFINITY表示
    for v in range(graph.vNum):
        dist[v]=graph.g[s][v]
        if dist[v] < float('inf'):#如果是原点的邻接点
            path[v] = s
        else:
            path[v]=-1
        
    #先将起点收入集合
    dist[s] = 0
    collected[s] = True

    while 1:
        v= findMinDist(graph,dist,collected) #v:未被收录顶点中dist最小者
        if v==None:
            break #若这样的V不存在,结束
        collected[v] = True

        for w in range(graph.vNum): #对图中的每个顶点W
            if collected[w] ==False and graph.g[v][w]<float('inf'):
                if graph.g[v][w]<0: #有负边
                    return False #不能解决
                if dist[v] + graph.g[v][w] < dist[w]: #经过已经收录的v到w的距离比现在w的距离小
                    dist[w]=dist[v]+graph.g[v][w]
                    path[w]=v
    print('path: ' + str(path))
    print('dist: ' + str(dist))
    return True


graph = Graph(7)
graph.insertEdge(Edge(0,1,2))
graph.insertEdge(Edge(0,3,1))
graph.insertEdge(Edge(1,3,3))
graph.insertEdge(Edge(1,4,10))
graph.insertEdge(Edge(2,0,4))
graph.insertEdge(Edge(2,6,5))
graph.insertEdge(Edge(3,2,2))
graph.insertEdge(Edge(3,4,2))
graph.insertEdge(Edge(3,6,4))
graph.insertEdge(Edge(3,5,8))
graph.insertEdge(Edge(4,6,6))
graph.insertEdge(Edge(6,5,1))

dist=[float('inf')]*7
path=[-1]*7
dijkastra(graph,dist,path,0)