n = int(input().strip())

plist=[]
for i in range(2):
    clist = list(map(int,input().strip().split(' ')))
    plist.append(clist)


lista=plist[0]
listb=plist[1]
num = 0
for i in range(len(lista)):
    for j in range(i+1, len(lista)):
        aChildList=lista[i:j]
        bChildList=listb[i:j]
        aChildMax = max(aChildList)
        bChildMin =min(bChildList)
        if aChildMax < bChildMin:
            num+=1

print(num)