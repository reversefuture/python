mnStr=input()
m,n = list(map(int, mnStr.strip().split(' ')))
sqr = []
for i in range(n):
    l = list(map(int,input().strip().split()))
    print("l: " + str(l))
    sqr.append(l)

def findSibling(lm,ln,label):
    if ln-1 >=0:
        if sqr[lm][ln-1] == 1:
            sqr[lm][ln-1] = label
            findSibling(lm,ln-1,label)
        if lm-1 >=0:
            if sqr[lm-1][ln-1] == 1:
                sqr[lm-1][ln-1] =label
                findSibling(lm-1,ln-1,label) 
        if lm+1 < m:
            if sqr[lm+1][ln-1] == 1:
                sqr[lm+1][ln-1] =label
                findSibling(lm+1,ln-1,label)               
    if ln+1 <n:
        if sqr[lm][ln+1] == 1:
            sqr[lm][ln+1] =label
            findSibling(lm,ln+1,label)
        if lm-1 >=0:
            if sqr[lm-1][ln+1] == 1:
                sqr[lm-1][ln+1] =label
                findSibling(lm-1,ln+1,label) 
        if lm+1 < m:
            if sqr[lm+1][ln+1] == 1:
                sqr[lm+1][ln+1] =label
                findSibling(lm+1,ln+1,label)    

    if lm-1 >=0:
        if sqr[lm-1][ln] == 1:
            sqr[lm-1][ln] =label
            findSibling(lm-1,ln,label)        
    if lm+1 <m:
        if sqr[lm+1][ln] == 1:
            sqr[lm+1][ln] =label
            findSibling(lm+1,ln,label)        
    return

fansList =[]
groupNum=0
for ln in range(n):
    for lm in range(m):
        if sqr[lm][ln] == 1:
            groupNum+=1
            label='g'+str(groupNum)
            sqr[lm][ln] = label
            findSibling(lm,ln, label)

print(sqr)
result={}
for ln in range(n):
    for lm in range(m):
        if sqr[lm][ln] in result:
            result[sqr[lm][ln]]+=1
        elif sqr[lm][ln] ==0:
            pass
        else:
            result[sqr[lm][ln]]=1

num = len(result.keys())
max = 0;
for k,v in result.items():
    if v>max:
        max=v
print('num: ' + str(num))
print('max: ' + str(max))
