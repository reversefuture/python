auditNum = int(input().strip())

d1 = {}
for i in range(auditNum):
    paraList = input().strip().split(';')
    for t in paraList:
        start, end = map(int,t.split(','))
        if start in d1:
            if end > d1[start]:
                d1[start] = end
        else:
             d1[start] = end
             
startList = list(d1.keys())
newStartList = []


def filterD1():
    global startList,newStartList,d1
    for j in range(len(startList)):
        if startList[j] not in d1:
            continue
        for l in range(j+1,len(startList)):
            if startList[l] not in d1:
                continue
            if startList[l] <= d1[startList[j]] and d1[startList[l]] > d1[startList[j]]:
                d1[startList[j]] = d1[startList[l]]
                d1.pop(startList[l])
            elif startList[l] < startList[j]:
                if d1[startList[l]] >= d1[startList[j]]:
                    d1.pop(startList[j])
                elif d1[startList[l]] < d1[startList[j]] and d1[startList[l]] > startList[j]:
                    d1[startList[l]] = d1[startList[j]]
                    d1.pop(startList[j])
    newStartList = list(d1.keys())
    mut = list(filter(lambda k: k not in newStartList, startList))
    print(mut)
    if len(mut) > 0:
        startList = newStartList
        newStartList = []
        filterD1()

filterD1()
print(d1)